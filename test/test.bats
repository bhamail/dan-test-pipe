#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/dan-test-pipe"}

  echo "Building image..."
  docker build -t ${DOCKER_IMAGE}:test .
}

@test "Run Nancy with modfile" {
    run docker run \
        -e NANCY_MODFILE=./test/no-vulnerabilities/go.sum \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

@test "Run Nancy with modfile and debug" {
    run docker run \
        -e DEBUG=true \
        -e NANCY_MODFILE=./test/no-vulnerabilities/go.sum \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

@test "Run Nancy with options" {
    run docker run \
        -e NANCY_OPTIONS=-quiet \
        -v $(pwd):$(pwd) \
        -w /test/go-project \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

@test "Run Nancy with custom version" {
    run docker run \
        -e NANCY_VERSION="v0.0.41" \
        -v $(pwd):$(pwd) \
        -w /test/go-project \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

@test "Run Nancy go list" {
    run docker run \
        -v $(pwd):$(pwd) \
        -w /test/go-project \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

@test "Run Nancy go list custom command" {
    run docker run \
        -e NANCY_GOLISTCOMMNAD="go list -m -u all" \
        -v $(pwd):$(pwd) \
        -w /test/go-project \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}
