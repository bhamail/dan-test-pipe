#!/usr/bin/env bash
#
# A test pipe for learnings
#
set -e

source "$(dirname "$0")/common.sh"
#cat "$(dirname "$0")/common.sh"

info "Executing the pipe..."

# Required parameters
#NAME=${NAME:?'NAME variable missing.'}

# Default parameters
DEBUG=${DEBUG:="false"}
NANCY_VERSION=${NANCY_VERSION:="v0.0.42"}
NANCY_PLATFORM=${NANCY_PLATFORM:="linux.amd64"}

NANCY_OPTIONS=${NANCY_OPTIONS:=""}
# modfile would normally default to "go.sum", but we use it here also to signal running nancy via a modfile vs go list
NANCY_MODFILE=${NANCY_MODFILE:=}
NANCY_GOLISTCOMMNAD=${NANCY_GOLISTCOMMNAD:="go list -m all"}

NANCY_BIN="/tmp/tools/nancy"

install_nancy()
{
  debug "Installing Nancy ${NANCY_VERSION}"
  mkdir /tmp/tools
  sourceUrl="https://github.com/sonatype-nexus-community/nancy/releases/download/${NANCY_VERSION}/nancy-${NANCY_PLATFORM}-${NANCY_VERSION}"
  if [[ ${sourceUrl} == *"windows"* ]]; then
    sourceUrl="${sourceUrl}.exe"
  fi
  debug "Getting Nancy ${sourceUrl}"
  curl --fail -s -L "${sourceUrl}" -o "${NANCY_BIN}"
  chmod +x "${NANCY_BIN}"
  debug "Installed Nancy as ${NANCY_BIN}"
}

run_nancy()
{
  info "Running Nancy with go list"
  ${NANCY_GOLISTCOMMNAD} | ${NANCY_BIN} ${NANCY_OPTIONS}
  debug "Ran Nancy with go list, options: ${NANCY_OPTIONS}"
  status=0
}

run_nancy_modfile()
{
  info "Runing Nancy with modfile ${NANCY_MODFILE}"
  ${NANCY_BIN} ${NANCY_OPTIONS} ${NANCY_MODFILE}
  debug "Ran Nancy with modfile, options: ${NANCY_OPTIONS}"
  status=0
}

install_nancy

if [ -z ${NANCY_MODFILE} ]
then
  run_nancy
else
  run_nancy_modfile
fi


if [[ "${status}" == "0" ]]; then
  success "Success!"
else
  fail "Error!"
fi
