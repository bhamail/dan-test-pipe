#FROM alpine:3.9
FROM golang:rc-alpine

RUN apk add --update --no-cache bash
RUN apk add --update --no-cache curl

COPY pipe /
COPY LICENSE.txt pipe.yml README.md /
COPY test /test
RUN wget -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.4.0/common.sh

RUN chmod a+x /*.sh

ENTRYPOINT ["/pipe.sh"]
